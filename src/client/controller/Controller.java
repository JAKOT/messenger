package client.controller;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import client.model.ClientModel;
import client.view.MainFrame;
import gui.addframe.AddFrame;
import gui.historyframe.HistoryFrame;
import gui.imagepreviewpanel.ImagePreviewPanel;
import gui.loginframe.LoginFrame;
import gui.messageframe.MessageFrame;
import gui.notificationframe.NotificationFrame;
import gui.settingsframe.SettingsFrame;
import gui.registerframe.RegisterFrame;

public class Controller {
	private MainFrame mainFrame;                 				// glowne okno programu
	private LoginFrame loginFrame;
	private RegisterFrame registerFrame;
	private AddFrame addFrame;
	private NotificationFrame notificationFrame;
	private HistoryFrame historyFrame;
	private SettingsFrame settingsFrame;
	private Map<String, MessageFrame> ChatFrames;

	private ClientModel clientModel;
	private String userNickSender;                    // nick usera który jest zalogowany w oknie gl. programu


	public Controller(ClientModel clientModel) {
		this.clientModel = clientModel;
		this.ChatFrames = new HashMap<String, MessageFrame>();
	}

	// GETTERS AND SETTERS
	public MainFrame getMainFrame() {
		return mainFrame;
	}

	public LoginFrame getLoginFrame() {
		return loginFrame;
	}

	public RegisterFrame getRegisterFrame() {
		return registerFrame;
	}

	public AddFrame getAddFrame() { return addFrame; }

	public NotificationFrame getNotificationFrame() { return notificationFrame; }

	public ClientModel getClientModel() { return clientModel; }

	public HistoryFrame getHistoryFrame() { return  historyFrame; }

	public SettingsFrame getSettingsFrame() { return settingsFrame; }

	public void setMainFrame(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
		this.mainFrame.addLogoutButtonListener(new LogoutButtonListener());
		this.mainFrame.addSendButtonListener(new SendBroadcastButtonListener());
		this.mainFrame.addFriendListListener(new FriendsListListener());
		clientModel.setMainFrame(mainFrame);
	}

	public void setSettingsFrame(SettingsFrame settingsFrame) {
		this.settingsFrame = settingsFrame;
		this.settingsFrame.addApplyButtonListener(new ApplyButtonListener());
	}

	public void setLoginFrame(LoginFrame loginFrame) {
		this.loginFrame = loginFrame;
		this.loginFrame.addLoginButtonListener(new LoginButtonListener());
	}

	public void setRegisterFrame(RegisterFrame registerFrame) {
		this.registerFrame = registerFrame;
		this.registerFrame.addRegisterButtonListener(new RegisterButtonListener());
	}

	public void setAddFrame(AddFrame addFrame) {
		this.addFrame = addFrame;
		this.addFrame.addaddButtonListener(new AddButtonListener());
		this.addFrame.addFileSelectorButtonListener(new FileSelectorButtonListener());
	}

	public void setHistoryFrame(HistoryFrame historyFrame) {
		this.historyFrame = historyFrame;
		this.historyFrame.addFriendListListener( new HistoryListListener());
		clientModel.setHistoryFrame(this.historyFrame);
		this.historyFrame.addExportButtonListener(new ExportButtonListener());
	}

	public void setNotificationFrame(NotificationFrame notificationFrame) {
		this.notificationFrame = notificationFrame;
	}

	public void addChatFrame(String name, MessageFrame frame) {
		ChatFrames.put(name, frame);
	}

	public void removeChatFrame(String name) {
		ChatFrames.remove(name);
	}

	public String getUserNick() {
		return userNickSender;
	}

	public void setUserNick(String userNick) {
		this.userNickSender = userNick;
	}

	//////////////////////////////////////////////////////////////

	public void closeFrames() {
		// Closing all the windows in case of error etc.
		if (mainFrame != null) {
			mainFrame.dispose();
			mainFrame = null;
		}
		if (settingsFrame != null) {
			settingsFrame.dispose();
			settingsFrame = null;
		}
		if (registerFrame != null) {
			registerFrame.dispose();
			registerFrame = null;
		}
		if (loginFrame != null) {
			loginFrame.dispose();
			loginFrame = null;
		}
		clientModel.closeChatFrames();
	}

	public void registerUser(String name, String password) {
		if (validateRegistration(name, password)) {
			clientModel.sendRegisterMessage(name, password);
			registerFrame.userRegisteredDialog();
		} else {
			registerFrame.invalidRegistrationDialog();
		}
	}

	public void createChatFrame(String friend) {
		Runnable runnable = () -> {
      MessageFrame frame = new MessageFrame(Controller.this, friend);
      frame.addSendButtonListener(new SendButtonListener());
			frame.addFileButtonListener(new FileButtonListener());
      clientModel.addChatFrame(friend, frame);
    };
		EventQueue.invokeLater(runnable);
	}

	public void createChatFrameWithMsg(String friend, String msg) {
		Runnable runnable = () -> {
      MessageFrame frame = new MessageFrame(Controller.this, friend, msg);
      frame.addSendButtonListener(new SendButtonListener());
			frame.addFileButtonListener(new FileButtonListener());
      clientModel.addChatFrame(friend, frame);
    };
		EventQueue.invokeLater(runnable);
	}

	public synchronized void loginUser(String name, String password) {
		clientModel.sendLoginData(name, password);
		mainFrame = new MainFrame(name, Controller.this);
		mainFrame.setVisible(true);
		userNickSender = name;
		loginFrame.setVisible(false);
		loginFrame.dispose();
		loginFrame = null;

		waitForResponse();

		if (!clientModel.isLogged()) {
			System.out.println("Not logged");
			clientModel.writeToFile("failed login attempt to: " + userNickSender, "log");
			mainFrame.dispose();
			mainFrame = null;
			loginFrame = new LoginFrame(Controller.this);
			loginFrame.setVisible(true);
			loginFrame.invalidLoginDialog();
			userNickSender = null;
		} else {
			setFriends();
			clientModel.writeToFile(userNickSender + " logged in", "log");
			waitForResponse();
			clientModel.writeToFile(userNickSender, "friends");
		}
	}

	public void addFriend(String friend) {
		clientModel.sendFriendRequest(userNickSender, friend);
		addFrame.clearFriendNameField();
		waitForResponse();
		setAddedFriendOnJList(friend);
	}

	public synchronized void waitForResponse() {
		try {
			wait(200);
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		}
	}

	public void setAddedFriendOnJList(String friend) {
		clientModel.askForFriends(userNickSender);
		clientModel.sendMessage(" added " + friend + " to friends.", userNickSender);
		refreshJList();
	}

	public void setFriends() {
		clientModel.askForFriends(userNickSender);
		clientModel.sendMessage(" logged in.", userNickSender);
		refreshJList();
	}

	public void refreshJList() {
		new Thread() { //need to wait first.
			@Override
			public void run() {
				try {
					Thread.sleep(100);
					Runnable runnable = () -> mainFrame.displayFriends(clientModel.getFriends());
					EventQueue.invokeLater(runnable);
				} catch (InterruptedException ex) {
					Thread.currentThread().interrupt();
				}
			}
		}.start();
	}

	public void logoutUser() {
		clientModel.closeChatFrames();
		loginFrame = new LoginFrame(mainFrame.getController());
		mainFrame.setVisible(false);
		mainFrame.dispose();
		mainFrame = null;
		loginFrame.setVisible(true);
		clientModel.logoutUser(userNickSender);
		userNickSender = null;
	}


	public void addFriendsFromFile(File file) {
		try {
			Scanner reader = new Scanner(file);
			while (reader.hasNext()) {
				String friend = reader.nextLine();
				System.out.println(friend);
				addFriend(friend);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}


	public boolean validateRegistration(String name, String password) {
		if (name.length() >= 5 && password.length() >= 5)
			return true;
		else
			return false;
	}

	class ExportButtonListener implements  ActionListener {
		public void actionPerformed(ActionEvent e) {
			clientModel.writeToFile(historyFrame.getFriendList().getSelectedValue(), "history");
		}
	}
	class SendBroadcastButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent actionEvent) {
			clientModel.sendMessage(mainFrame.getMessage(), userNickSender);
		}
	}

	class SendButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JButton button = (JButton) e.getSource();
			MessageFrame frame = (MessageFrame) button.getTopLevelAncestor();
			String to = frame.getFriend();
			String from = userNickSender;
			String msg = frame.getMessage();
			clientModel.sendChatMessage(to, from, msg);
			frame.getMessageArea().setText("");
			frame.addMessage(msg, userNickSender);
		}
	}

	class FileButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JButton button = (JButton) e.getSource();
			MessageFrame frame = (MessageFrame) button.getTopLevelAncestor();
			String to = frame.getFriend();
			String from = userNickSender;
			String msg = frame.getMessage();

			JFileChooser chooser = new JFileChooser();
			ImagePreviewPanel preview = new ImagePreviewPanel();
			chooser.setAccessory(preview);
			chooser.addPropertyChangeListener(preview);
			chooser.showOpenDialog(null);
			File file = chooser.getSelectedFile();

			try {
				clientModel.sendFile(file, to, from);
				System.out.println("File sent:" + file.getName());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	class FriendsListListener implements ListSelectionListener {
		public void valueChanged(ListSelectionEvent e) {
			if (e != null && !e.getValueIsAdjusting()) {
				if (mainFrame.getFriendsList().getSelectedValue() != null) {
					String choosenFriendNick = mainFrame.getFriendsList().getSelectedValue().toString();
					mainFrame.getFriendsList().clearSelection();

					// Opens chat window if it is not opened yet.
					if (!ChatFrames.containsKey(choosenFriendNick)) {
						createChatFrame(choosenFriendNick);
					} else {   // If it is opened already it brings it to the top.
						ChatFrames.get(choosenFriendNick).setVisible(true);
						mainFrame.getFriendsList().clearSelection();
					}
				}
			}
		}
	}

	class ApplyButtonListener implements  ActionListener {
		public void actionPerformed(ActionEvent e) {
			String nick = settingsFrame.getNameField().getText();
			String pswd = settingsFrame.getPasswordField().getText();
			if (!userNickSender.equals(nick) && nick.length() > 4 && pswd.length() > 4) {
				clientModel.sendFullUpdate(userNickSender, nick, pswd);
				JOptionPane.showMessageDialog(null, "Account updated.", "", JOptionPane.INFORMATION_MESSAGE);
			} else if (!nick.equals(userNickSender)) {
				clientModel.sendNickUpdate(userNickSender, nick);
				JOptionPane.showMessageDialog(null, "Nick updated.", "", JOptionPane.INFORMATION_MESSAGE);

			} else if (pswd.length() > 4) {
				clientModel.sendPswdUpdate(userNickSender, pswd);
				JOptionPane.showMessageDialog(null, "Password updated.", "", JOptionPane.INFORMATION_MESSAGE);
			} else if (pswd.length() != 0 && pswd.length() < 4 && nick.length() < 4) {
				JOptionPane.showMessageDialog(null, "Invalid update data.", "Error", JOptionPane.ERROR_MESSAGE);
			}

			JCheckBox cb = settingsFrame.getNotificationsCheckBox();
			if (cb.isSelected()) {
				getClientModel().setNotifications(true);
			} else {
				getClientModel().setNotifications(false);
			}
			settingsFrame.dispose();
			settingsFrame = null;
		}
	}

	class HistoryListListener implements ListSelectionListener {
		public void valueChanged(ListSelectionEvent evt) {
			if (evt != null && !evt.getValueIsAdjusting()) {
				if (historyFrame.getFriendList().getSelectedValue() != null) {
					String choosenFriendNick = historyFrame.getFriendList().getSelectedValue().toString();

					historyFrame.clearHistoryScrollPane();
					clientModel.askForHistoryWith(choosenFriendNick, userNickSender);
				}
			}
		}
	}

	class FileSelectorButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent actionEvent) {
			JFileChooser chooser = new JFileChooser();
			chooser.showOpenDialog(null);
			File file = chooser.getSelectedFile();
			addFriendsFromFile(file);
		}
	}

	class AddButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent actionEvent) {
			addFriend(addFrame.getFriendNameField());
		}
	}

	class RegisterButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			registerUser(registerFrame.getNameField(), registerFrame.getPasswordField());
		}
	}
	class LoginButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			loginUser(loginFrame.getNameField(), loginFrame.getPasswordField());
		}
	}
	class LogoutButtonListener implements MouseListener {
		public void mouseClicked(MouseEvent e) {
			logoutUser();
		}

		public void mousePressed(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
	}
}
