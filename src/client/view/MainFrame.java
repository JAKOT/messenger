package client.view;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.DefaultCaret;

import client.controller.Controller;

import gui.addframe.AddFrame;
import gui.historyframe.HistoryFrame;
import gui.loginframe.LoginFrame;
import gui.messageframe.MessageFrame;
import gui.registerframe.RegisterFrame;
import gui.settingsframe.SettingsFrame;

public class MainFrame extends JFrame {
	static JPanel boxPanel = new JPanel();
	private Controller controller;
	private JButton SendButton;
	private JButton FileButton;
	private JList<String> FriendsList;
	private JList<String> jList2;
	private javax.swing.JMenu jMenu1;
	private javax.swing.JMenu jMenu2;
	private javax.swing.JMenu jMenu3;
	private javax.swing.JMenu LogoutMenu;
	private javax.swing.JMenu jMenu5;
	private javax.swing.JMenu jMenu6;
	private javax.swing.JMenuBar jMenuBar1;
	private javax.swing.JMenuBar jMenuBar2;
	private javax.swing.JMenuItem jMenuItem1;
	private javax.swing.JMenuItem jMenuItem3;
	private javax.swing.JMenuItem jMenuItem4;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JScrollPane jScrollPane3;
	private javax.swing.JScrollPane jScrollPane4;
	private javax.swing.JScrollPane jScrollPane5;
	private javax.swing.JScrollPane jScrollPane6;
	private javax.swing.JTextArea jTextArea1;
	private javax.swing.JTextArea MessageArea;
	private java.awt.Menu menu1;
	private java.awt.Menu menu2;
	private java.awt.MenuBar menuBar1;
	private javax.swing.JLabel nickname;
	private java.awt.TextArea textArea1;

	public MainFrame(String nick, Controller controller) {
		initComponents();
		centerComponent(this);
		this.setResizable(false);
		MessageArea.setLineWrap(true);
		nickname.setText(nick);
		String name = nickname.getText();
		boxPanel.setLayout(new BoxLayout(boxPanel, BoxLayout.PAGE_AXIS));
		boxPanel.setVisible(true);
		this.setTitle("Communicator");
		setController(controller);
		getController().setMainFrame(this);

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent) {
				controller.logoutUser();
				super.windowClosing(windowEvent);
			}
		});
	}

	public void setController(Controller controller) {
		this.controller = controller;
	}

	public Controller getController() {
		return controller;
	}

	public JList getFriendsList() {
		return FriendsList;
	}

	public void centerComponent(Component frame) {
		Toolkit tk = Toolkit.getDefaultToolkit();
		Dimension dim = tk.getScreenSize();
		int xPos = (dim.width / 2) - (frame.getWidth() / 2);
		int yPos = (dim.height / 2) - (frame.getHeight() / 2);
		frame.setLocation(xPos, yPos);
	}

	private void initComponents() {

		jScrollPane2 = new JScrollPane();
		jScrollPane3 = new JScrollPane();
		jList2 = new JList<>();
		jScrollPane4 = new JScrollPane();
		jTextArea1 = new JTextArea();
		menuBar1 = new MenuBar();
		menu1 = new Menu();
		menu2 = new Menu();
		jMenuItem1 = new JMenuItem();
		jMenuBar2 = new JMenuBar();
		jMenu1 = new JMenu();
		jMenu2 = new JMenu();
		jMenuItem3 = new JMenuItem();
		textArea1 = new TextArea();
		jScrollPane1 = new JScrollPane();
		FriendsList = new JList<>();
		nickname = new JLabel();
		jScrollPane5 = new JScrollPane();
		MessageArea = new JTextArea();
		jScrollPane6 = new JScrollPane(boxPanel);
		jMenuBar1 = new JMenuBar();
		LogoutMenu = new JMenu();
		jMenu6 = new JMenu();
		jMenu3 = new JMenu();
		jMenuItem4 = new JMenuItem();
		jMenu5 = new JMenu();

		SendButton = new JButton();
		SendButton.setText("Send");
		FileButton = new JButton();
		FileButton.setText("Add file");

		jList2.setModel(new AbstractListModel<String>() {
			String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
			public int getSize() { return strings.length; }
			public String getElementAt(int i) { return strings[i]; }
		});
		jScrollPane3.setViewportView(jList2);

		jTextArea1.setColumns(20);
		jTextArea1.setRows(5);
		jScrollPane4.setViewportView(jTextArea1);

		menu1.setLabel("File");
		menuBar1.add(menu1);

		menu2.setLabel("Edit");
		menuBar1.add(menu2);

		jMenu1.setText("File");
		jMenuBar2.add(jMenu1);

		jMenu2.setText("Edit");
		jMenuBar2.add(jMenu2);

		jMenuItem3.setText("jMenuItem3");

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setMinimumSize(new Dimension(600, 400));
		setSize(new Dimension(700, 500));

		FriendsList.setBackground(new java.awt.Color(230, 230, 230));
		FriendsList.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(210, 210, 210)));
		FriendsList.setModel(new AbstractListModel<String>() {
			String[] strings = { "Jakub", "Dawid", "Marcin", "Krzysztof", "Leszke", "Anotherone", "Contact", "Yo mom" , "Kopaczkens", "Karczi"};
			public int getSize() { return strings.length; }
			public String getElementAt(int i) { return strings[i]; }
		});
		FriendsList.setFixedCellWidth(120);


		jScrollPane1.setViewportView(FriendsList);

		nickname.setFont(new Font("Ubuntu 15 Plain", 1, 16));
		nickname.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		nickname.setText("MyNick");

		MessageArea.setColumns(10);
		MessageArea.setRows(3);
		MessageArea.setWrapStyleWord(true);
		MessageArea.setMinimumSize(new Dimension(130, 30));
		jScrollPane5.setViewportView(MessageArea);

		jScrollPane6.setBackground(new java.awt.Color(162, 174, 174));
		jScrollPane6.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		jScrollPane6.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		jScrollPane6.setFocusable(false);
		jScrollPane6.setVerifyInputWhenFocusTarget(false);

		LogoutMenu.setText("Log out");
		jMenuBar1.add(LogoutMenu);

		jMenu6.setText("Settings");
		jMenuBar1.add(jMenu6);

		jMenu3.setText("Profile");

		jMenuItem4.setText("History");
		jMenuItem4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jMenuItem4ActionPerformed(evt);
			}

		});

		jMenu3.add(jMenuItem4);

		jMenuBar1.add(jMenu3);

		jMenu5.setText("Add");
		jMenu5.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				jMenu5MouseClicked(evt);
			}
		});

		jMenu6.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				jMenu6MouseClicked(evt);
			}
		});
		jMenuBar1.add(jMenu5);

		setJMenuBar(jMenuBar1);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
			layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addComponent(nickname, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(layout.createSequentialGroup()
							.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
							.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(layout.createSequentialGroup()
									.addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 520, javax.swing.GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
										.addComponent(FileButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(SendButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
									.addGap(0, 4, Short.MAX_VALUE))
								.addComponent(jScrollPane6))))
					.addContainerGap())
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(nickname, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
					.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
							.addComponent(jScrollPane6)
							.addGap(18, 18, 18)
							.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
								.addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGroup(layout.createSequentialGroup()
									.addComponent(SendButton)
									.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(FileButton))))
						.addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE))
					.addGap(15, 15, 15))
		);

		pack();
	}

	private void jMenuItem4ActionPerformed(ActionEvent evt) {
		HistoryFrame history = new HistoryFrame(controller);
	}

	public void addLogoutButtonListener(MouseListener listenForLogoutButton){
		LogoutMenu.addMouseListener(listenForLogoutButton);
	}

	public void addSendButtonListener(ActionListener listenForSendBroadcastButton) {
		SendButton.addActionListener(listenForSendBroadcastButton);
	}

	public void addFriendListListener(ListSelectionListener listenForFriendList) {
		FriendsList.addListSelectionListener(listenForFriendList);
	}

	public String getMessage() {
		return MessageArea.getText().trim();
	}

	public void setMessage(String text) {
		MessageArea.setText(text);
	}

	public void addMessage(String msg, String nick) {
		if(msg.length()!=0) {
			JLabel label = new JLabel();
			label.setOpaque(true);
			label.setBackground(new Color(240, 240, 240));
			label.setText("<html><p style=\"width:400px;\">" + "[<span style=\"color:#00b3b3;font-weight:bold;\">" + nick + "</span>] " + msg + "</p></html>");
			label.setForeground(Color.BLACK);
			boxPanel.add(label);
			boxPanel.add(Box.createRigidArea(new Dimension(0,5)));
			MessageArea.setText("");
			boxPanel.revalidate();
			final JScrollBar verticalBar = jScrollPane6.getVerticalScrollBar();
			AdjustmentListener downScroller = new AdjustmentListener() {
				public void adjustmentValueChanged(AdjustmentEvent e) {
					Adjustable adjustable = e.getAdjustable();
					adjustable.setValue(adjustable.getMaximum());
					verticalBar.removeAdjustmentListener(this); // need this so it does not stop user from scrolling up
				}
			};
			verticalBar.addAdjustmentListener(downScroller);
		}
	}

	private void jMenu5MouseClicked(MouseEvent evt) {

		//Tworzymy nowe okno jeżeli nei jest już otwarte
		if (controller.getAddFrame() == null) {
			AddFrame add = new AddFrame(controller);
			add.setVisible(true);
		} else {  // jeżeli jest otwarte - przenosimy je na inne okienka
			controller.getAddFrame().setVisible(true);
		}
	}

	private void jMenu6MouseClicked(MouseEvent evt) {
		SettingsFrame SettingsFrame = new SettingsFrame(controller);
		SettingsFrame.setVisible(true);
	}


	public void displayFriends(ArrayList<String> Friends) {
		DefaultListModel list = new DefaultListModel();
		for (String friend : Friends) {
			list.addElement(friend);
		}
		//FriendsList.setModel(list);

		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				FriendsList.setModel(list);
				FriendsList.setCellRenderer(new MyCellRender());
			}
		};
		EventQueue.invokeLater(runnable);
	}

	private class MyCellRender extends DefaultListCellRenderer {
		@Override
		public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
			super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
			if (controller.getClientModel().getLoggedInFriends().contains(value.toString())) {
				setBackground(new Color(0, 204, 204));
				setFont(new Font("Areal", Font.BOLD,13));
				setForeground(Color.white);
			} else {
				setFont(new Font("Areal", Font.BOLD,13));
			}
			return this;
		}
	}
}
