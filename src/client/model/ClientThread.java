package client.model;

import client.view.MainFrame;

import java.net.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

import javax.swing.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Math.toIntExact;

public class ClientThread extends Thread {
	private Socket           socket   = null;
	private ClientModel  client   = null;
	private DataInputStream  inStream = null;
	private boolean flag = false;

	public ClientThread(ClientModel _client, Socket _socket) {
		client   = _client;
		socket   = _socket;
		flag = true;
		open();
		start();
	}

	public void open() {
		try {
			inStream = new DataInputStream(socket.getInputStream());
		} catch(IOException ioe) {
			ioe.printStackTrace();
			client.stop();
		}
	}
	public void close() {
		try {
			if (inStream != null) inStream.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	public void run() {
		System.out.println("Client thread started !");
		while (flag) {
			try {
				int incoming = inStream.readInt();
				System.out.println("Incoming to client: " + incoming);
				switch (incoming) {
					case 2: {                                        // 2 for successful login info.
						boolean bool = inStream.readBoolean();
						client.setLogged(true);
						System.out.println(" INCOMING = 2, bool: " + bool + " Set logged in to " + true);
						break;
					}
					case 3: {																				 // 3 for failed login info.
						boolean bool = inStream.readBoolean();
						client.setLogged(false);
						System.out.println(" INCOMING = 3, bool: " + bool + " Set logged in to " + false);
						break;
					}
					case 4: {																				//4 for receiving broadcast msg/
						String msg = inStream.readUTF();
						String nick = inStream.readUTF();
						System.out.println("Incoming msg: " + msg + " from " + nick);
						client.readMessage(msg, nick);
						break;
					}
					case 7: {                                        // 7 for receiving friend list.
						String friend = inStream.readUTF();
						while (!friend.equals("end")) {
							if (client.isNotInFriends(friend)) client.addFriend(friend);
							friend = inStream.readUTF();
						}
						System.out.println(client.getFriends());
						break;
					}
					case 8: {
						String sender = inStream.readUTF();
						String msg = inStream.readUTF();
						System.out.println("Client received Priv Chat message from: " + sender + " msg: " + msg);
						client.readChatMessage(msg, sender);
						break;
					}
					case 9: {
						client.setHistory(new HashMap<>());
						String content = inStream.readUTF();
						while (!content.equals("end")) {
							String sender = inStream.readUTF();
							String sent = inStream.readUTF();
							client.putIntoHistory(content, sent);
							client.getHistoryFrame().addMessage(content, sender, sent);
							content = inStream.readUTF();
						}
						break;
					}
					case 10: {
						client.setLoggedInFriends(new ArrayList<>());
						String friend = inStream.readUTF();
						while (!friend.equals("end")) {
							client.addLoggedInFriend(friend);
							friend = inStream.readUTF();
						}
						client.getMainFrame().getFriendsList().repaint();
						break;
					}
					case 11: {
						System.out.println("Client received Send file request.");
						String sender = inStream.readUTF();
						Long fileSize = inStream.readLong();
						String fileName = inStream.readUTF();
						int confirm = JOptionPane.showConfirmDialog(null, "From: " + sender + "\nFilename: "+ fileName + "\nwould you like to Accept.?");
						if(confirm == 0) { // client accepted the request, then inform the sender to send the file now
							FileOutputStream fos = new FileOutputStream(fileName);
							byte[] buffer = new byte[toIntExact(fileSize)];
							int read;
							int totalRead = 0;
							int remaining = toIntExact(fileSize);
							while (remaining != 0) {
								read = inStream.read(buffer, 0, Math.min(buffer.length, remaining));
								totalRead += read;
								remaining -= read;
								fos.write(buffer, 0, read);
							}

							fos.close();
						} else { // clienr rejected file
							byte[] buffer = new byte[toIntExact(fileSize)];
							inStream.readFully(buffer);
						}

						if (fileName.toLowerCase().endsWith(".gif") || fileName.toLowerCase().endsWith(".jpg") ||
								fileName.toLowerCase().endsWith(".jpeg") || fileName.toLowerCase().endsWith(".png")) {
							client.getChatFrameWith(sender).addImage(fileName, sender);
						} else {
							client.getChatFrameWith(sender).addMessage(fileName + " saved to " + (new File(fileName).getAbsolutePath()), sender);
						}

						break;
					}
					default: {
						System.out.println("CLIENT ERROR");
						client.writeToFile("Client thread switch error.", "log");
						break;
					}
				}
			} catch (EOFException e) {
				flag = false;
				client.getMainFrame().getController().closeFrames();
				close();
				client.stop();
			} catch  (IOException ioe) {
				flag = false;
				client.getMainFrame().getController().closeFrames();
				ioe.printStackTrace();
				client.writeToFile(ioe.getMessage(), "log");
				close();
				client.stop();
			}
		}
	}
}
