package client.model;

import client.controller.Controller;
import client.view.MainFrame;
import gui.Countdown;
import gui.historyframe.HistoryFrame;
import gui.messageframe.MessageFrame;
import gui.notificationframe.NotificationFrame;
import javafx.embed.swing.JFXPanel;
import server.Message;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.io.*;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.*;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;


import javafx.scene.media.MediaPlayer;


public class ClientModel {
	private Socket socket             		= null;
	private DataInputStream  inStream  		= null;
	private DataOutputStream outStream 		= null;
	private ClientThread client 					= null;
	private MainFrame mainFrame = null;
	private boolean logged = false;
	private ArrayList<String> Friends = new ArrayList<>();
	private Map<String, MessageFrame> ChatFrames;
	private ArrayList<String> LoggedInFriends = new ArrayList<>();
	private static Countdown countdown;
	private NotificationFrame notificationFrame = null;
	private HistoryFrame historyFrame = null;
	private boolean notifications = true;
	private Map<String, String> history;

	public ClientModel(String serverName, int serverPort) {
		ChatFrames = new HashMap<>();
		try {
			socket = new Socket(serverName, serverPort);
			System.out.println("Connected: " + socket);
			start();
		} catch(UnknownHostException uhe) {
			uhe.printStackTrace();
			writeToFile("Client opening socket UnknownHostException: " + uhe.getMessage(), "log");
			stop();
		} catch (IOException ioe) {
			ioe.printStackTrace();
			writeToFile("Client opening socket IOException: " + ioe.getMessage(), "log");
			stop();
		}
	}

	public ArrayList<String> getFriends() {
		return this.Friends;
	}

	public MainFrame getMainFrame() { return mainFrame; }

	public void addFriend(String friend) {
		Friends.add(friend);
	}

	public void addChatFrame(String friendNick, MessageFrame frame) {
		ChatFrames.put(friendNick, frame);
	}

	public void setMainFrame(MainFrame frame) {
		this.mainFrame = frame;
	}

	public void setHistoryFrame(HistoryFrame frame) { this.historyFrame = frame; }

	public boolean getNotifications() {
		return notifications;
	}

	public void addLoggedInFriend(String name) { LoggedInFriends.add(name); }

	public void setNotifications(boolean bool) {
		notifications = bool;
	}

	public void setLoggedInFriends(ArrayList<String> list) { LoggedInFriends = list; }

	public ArrayList<String> getLoggedInFriends() { return LoggedInFriends; }

	public Map<String, String> getHistory() { return this.history; }

	public void setHistory(HashMap<String, String> map) { history = map; }

	public void putIntoHistory(String k, String v) {
		this.history.put(k, v);
	}

	public HistoryFrame getHistoryFrame() { return this.historyFrame; }

	public MessageFrame getChatFrameWith(String nick) { return ChatFrames.get(nick); }

	public void sendRegisterMessage(String name, String password) {
		try{
			outStream.writeInt(1);
			outStream.writeUTF(name);
			outStream.writeUTF(password);
			outStream.flush();
		}catch(IOException ioe){
			ioe.printStackTrace();
			System.out.println("Error sending registration data: " + ioe.getMessage());
			writeToFile("Error sending registration data: " + ioe.getMessage(), "log");
			stop();
		}
	}

	public void sendNickUpdate(String oldOne, String newOne) {
		try{
			outStream.writeInt(12);
			outStream.writeUTF(oldOne);
			outStream.writeUTF(newOne);
			outStream.flush();
		}catch(IOException ioe){
			ioe.printStackTrace();
			System.out.println("Error sending nick update data: " + ioe.getMessage());
			writeToFile("Error sending nick update data: " + ioe.getMessage(), "log");
			logoutUser(mainFrame.getController().getUserNick());
			stop();
		}
	}

	public void sendPswdUpdate(String nick, String pswd) {
		try{
			outStream.writeInt(13);
			outStream.writeUTF(nick);
			outStream.writeUTF(pswd);
			outStream.flush();
		}catch(IOException ioe){
			ioe.printStackTrace();
			System.out.println("Error sending password update data: " + ioe.getMessage());
			writeToFile("Error sending password update data: " + ioe.getMessage(), "log");
			logoutUser(mainFrame.getController().getUserNick());
			stop();
		}
	}

	public void sendFullUpdate(String oldOne, String newOne, String pswd) {
		try{
			outStream.writeInt(14);
			outStream.writeUTF(oldOne);
			outStream.writeUTF(newOne);
			outStream.writeUTF(pswd);
			outStream.flush();
		}catch(IOException ioe){
			ioe.printStackTrace();
			System.out.println("Error sending full update data: " + ioe.getMessage());
			writeToFile("Error sending full update data: " + ioe.getMessage(), "log");
			logoutUser(mainFrame.getController().getUserNick());
			stop();
		}
	}

	public void setNotificationFrame(NotificationFrame frame) {
		notificationFrame = frame;
	}

	public boolean isNotInFriends(String friend) {
		for (String user : Friends) {
			if (user.equals(friend)) return false;
		}
		return true;
	}

	public void sendLoginData(String name, String password) {
		try {
			outStream.writeInt(2);
			outStream.writeUTF(name);
			outStream.writeUTF(password);
			outStream.flush();
		} catch ( IOException ioe) {
			System.out.println("Error sending login data: " + ioe.getMessage());
			writeToFile("Error sending login data: " + ioe.getMessage(), "log");
			stop();
		}
	}

	public void logoutUser(String nick) {
		this.logged = false;
		try {
			outStream.writeInt(5);
			outStream.writeUTF(nick);
			outStream.flush();
			Friends.clear();
		} catch (IOException ioe) {
			System.out.println("Error sending logout data: " + ioe.getMessage());
			writeToFile("Error sending logout data: " + ioe.getMessage(), "log");
			stop();
		}
	}
	public synchronized void setLogged(boolean val) {
		this.logged = val;
	}

	public boolean isLogged() {
		return this.logged;
	}

	public void start() throws IOException {
		inStream = new DataInputStream(System.in);
		outStream = new DataOutputStream(socket.getOutputStream());

		client = new ClientThread(this, socket);
	}

	public void stop() {
		try {
			if (inStream   != null)  inStream.close();
			if (outStream != null)  outStream.close();
			if (socket    != null)  socket.close();
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}
		client.close();
		client.interrupt();
		return;
	}

	public void sendMessage(String msg, String nick) {
		try {
			outStream.writeInt(4);
			outStream.writeUTF(msg);
			outStream.writeUTF(nick);
			outStream.flush();
		} catch ( IOException ioe) {
			System.out.println("Error sending broadcast message data: " + ioe.getMessage());
			writeToFile("Error sending broadcast message data: " + ioe.getMessage(), "log");
		}
	}

	public void sendFriendRequest(String user, String friend) {
		try {
			outStream.writeInt(6);
			outStream.writeUTF(user);
			outStream.writeUTF(friend);
			outStream.flush();
		} catch ( IOException ioe) {
			System.out.println("Error sending friend request data: " + ioe.getMessage());
			writeToFile("Error sending friend request data: " + ioe.getMessage(), "log");
			logoutUser(mainFrame.getController().getUserNick());
			stop();
		}
	}

	public void readMessage(String msg, String nick) {
				mainFrame.addMessage(msg, nick);
	}

	public void readChatMessage(String msg, String sender) {
		if (ChatFrames.containsKey(sender)) {
			ChatFrames.get(sender).addMessage(msg, sender);
		} else {
			Controller controller = mainFrame.getController();
			controller.createChatFrameWithMsg(sender, msg);
			displayNotification(sender);
			playSound();
			new Countdown(notificationFrame, this);
		}
		if (ChatFrames.get(sender) != null && (ChatFrames.get(sender).getState() != Frame.NORMAL)) {
			if (notificationFrame == null) {
				displayNotification(sender);
				playSound();
				new Countdown(notificationFrame, this);
			} else {
				notificationFrame.setText("Received message from " + sender);
				playSound();
				new Countdown(notificationFrame, this);
			}
		}
	}

	public void displayNotification(String nick) {
		notificationFrame = new NotificationFrame();
		notificationFrame.setText("Received message from " + nick);
		notificationFrame.addNotificationListener(new MouseInputAdapter() {
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
				ChatFrames.get(nick).setExtendedState(JFrame.MAXIMIZED_BOTH);
				ChatFrames.get(nick).setVisible(true);
				notificationFrame.dispose();
			}
		});
	}


	public void removeChatFrame(String nick) {
		if (ChatFrames.containsKey(nick)) {
			ChatFrames.remove(nick);
			System.out.println("Chat frame remvoed: " + nick);
		}
	}

	private void playSound() 	{
		if (notifications) {
			new Thread() {
				public void run() {
					new JFXPanel();
					final URL resource = getClass().getResource("bing.wav");
					Media hit = new Media(new File("bing.wav").toURI().toString());
					MediaPlayer mediaPlayer = new MediaPlayer(hit);
					mediaPlayer.play();
				}
			}.start();
		}
	}

	public void askForFriends(String user) {
		try {
			outStream.writeInt(7);
			outStream.writeUTF(user);
			outStream.flush();
		} catch ( IOException ioe) {
			System.out.println("Error sending getFriends(user) data " + ioe.getMessage());
			writeToFile("Error sending getFriends(user) data " + ioe.getMessage(), "log");
			logoutUser(mainFrame.getController().getUserNick());
			stop();
		}
	}

	public void askForHistoryWith(String friend, String user) {
		try {
			outStream.writeInt(9);
			outStream.writeUTF(user);
			outStream.writeUTF(friend);
			outStream.flush();
		} catch ( IOException ioe) {
			System.out.println("Error sending askForHistory(friend, user) data " + ioe.getMessage());
			writeToFile("Error sending askForHistory(friend, user) data " + ioe.getMessage(), "log");
			logoutUser(mainFrame.getController().getUserNick());
			stop();
		}
	}

	public void sendChatMessage(String to, String from, String msg) {
		try {
			outStream.writeInt(8);
			outStream.writeUTF(to);
			outStream.writeUTF(from);
			outStream.writeUTF(msg);
			outStream.flush();
		} catch ( IOException ioe) {
			System.out.println("Error sending ChatMessage " + ioe.getMessage());
			writeToFile("Error sending ChatMessage " + ioe.getMessage(), "log");
			logoutUser(mainFrame.getController().getUserNick());
			stop();
		}
	}

	public void sendFile(File file, String recipient, String sender) throws IOException {
		FileInputStream fis = new FileInputStream(file);
		byte[] buffer = new byte[(int) file.length()];

		outStream.writeInt(11);                // 11 for sending a file.
		outStream.writeUTF(recipient);
		outStream.writeUTF(sender);
		outStream.writeLong(file.length());     // sending file size.
		outStream.writeUTF(file.getName());
		while (fis.read(buffer) > 0) {
			outStream.write(buffer);
		}

		String fileName = file.getName();
		if (fileName.toLowerCase().endsWith(".gif") || fileName.toLowerCase().endsWith(".jpg") ||
			fileName.toLowerCase().endsWith(".jpeg") || fileName.toLowerCase().endsWith(".png")) {
			getChatFrameWith(recipient).addImageWithPath(file.getAbsolutePath(), mainFrame.getController().getUserNick());
		} else {
			getChatFrameWith(recipient).addMessage(file.getName() + " sent.", mainFrame.getController().getUserNick());
		}

		fis.close();
	}


	public void writeToFile(String line, String type) {
		if (type.equals("log")) {
			try (
					FileWriter fw = new FileWriter("messenger.log", true);
					BufferedWriter bw = new BufferedWriter(fw);
					PrintWriter out = new PrintWriter(bw)
				 )
			{
				Date date = new Date();
				String toWrite = "[" + date.toString() + "] " + line;
				out.println(toWrite);
			} catch (IOException ioe) {
				System.err.println("IOException: " + ioe.getMessage());
			}
		} else if (type.equals("friends")) {
			try (
				FileWriter fw = new FileWriter("friend_list", false);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter out = new PrintWriter(bw)
			)
			{
				for (String friend : Friends) {
					out.println(friend);
				}
			} catch (IOException ioe) {
				System.err.println("IOException: " + ioe.getMessage());
			}
		} else if (type.equals("history")) {
			try (
				FileWriter fw = new FileWriter("history_with_" + line, false);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter out = new PrintWriter(bw)
			)
			{
				for (Map.Entry<String, String> entry : history.entrySet()) {
					String key = entry.getKey();
					String value = entry.getValue();
					out.println("[" + line + ":" + value + "] " + key);
				}
			} catch (IOException ioe) {
				System.err.println("IOException: " + ioe.getMessage());
			}
		}
	}

	public void closeChatFrames() {
		Iterator it = ChatFrames.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry)it.next();
			System.out.println(pair.getKey() + " = " + pair.getValue());
			MessageFrame frame = (MessageFrame) pair.getValue();
			ChatFrames.remove(frame);
			frame.dispose();
			it.remove(); // avoids a ConcurrentModificationException
		}
	}
}
