package test;
import client.controller.Controller;
import client.model.ClientModel;
import gui.loginframe.LoginFrame;
import server.MessengerServer;

import javax.swing.*;
import java.awt.*;

public class ClientTest {
	public static void main(String[] args) {

		MessengerServer server = new MessengerServer(8190);
		
		ClientModel client = new ClientModel("localhost", 8190);

		final Controller controller = new Controller(client);
		
		
		Runnable runnable = new Runnable() {
			public void run() {
				LoginFrame loginFrame = new LoginFrame(controller);
				try {
					for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
						if ("com.sun.java.swing.plaf.gtk.GTKLookAndFeel".equals(info.getClassName())) {
							javax.swing.UIManager.setLookAndFeel(info.getClassName());
							break;
						}
					}
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
					e.printStackTrace();
				}
				loginFrame.setVisible(true);
			}
		};
		EventQueue.invokeLater(runnable);
		System.out.println("Widok - OK");

	}
}
