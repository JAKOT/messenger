package server;

import java.io.Serializable;

public class Message implements Serializable {
	private String content;
	private String from;
	private String to;
	
	public String getContent() { return this.content; }
	public void setContent(String content) { this.content = content; }
}
