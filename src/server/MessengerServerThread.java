package server;
import java.net.*;
import java.sql.SQLException;
import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import static java.lang.Math.toIntExact;

public class MessengerServerThread extends Thread {
	private MessengerServer 	server 		= null;
	private Socket 						socket 		= null;
	private int               ID        = -1;
	private DataInputStream   inStream  =  null;
	private DataOutputStream  outStream = null;
	private boolean 					flag 			= false; // Flaga warunkowa do działania wątku servera dla danego clienta
	
	public MessengerServerThread(MessengerServer _server, Socket _socket) {
		super();
		server = _server;
		socket = _socket;
		ID		 = socket.getPort();
		this.flag = true;
	}
	public void send(String msg, String nick) {
		try {
			outStream.writeInt(4);
			outStream.writeUTF(msg);
			outStream.writeUTF(nick);
			outStream.flush();
		} catch (IOException ioe) {
			ioe.printStackTrace();
	 		server.remove(ID);
	 		return;
		}
	}
	public int getID() { return ID; }
	public void run() {
		System.out.println("Server Thread " + ID + " running.");
		while (flag) {
			try {
				int incoming = inStream.readInt();

				System.out.println("Incoming: " + incoming);
				switch (incoming) {
					case 1: {                                                                  	 // 1 for registration data.
						server.registerUser(inStream.readUTF(), inStream.readUTF());
						break;
					}
					case 2: { 																																		// 2 for login data.
						String name = inStream.readUTF();
						String password = inStream.readUTF();
						boolean valid = server.checkUser(name, password);
						System.out.println(valid);
						if (valid) {
							server.getMapUserConnectionOutputStreams().put(name, outStream);
							outStream.writeInt(2);
							outStream.writeBoolean(true);
							outStream.flush();
						} else {
							outStream.writeInt(3);
							outStream.writeBoolean(false);
							outStream.flush();
						}
						break;
					}
					case 4: {																																			// 4 for broadcast msg data.
						server.broadcast(inStream.readUTF(), inStream.readUTF());
						break;
					}
					case 5: {                                                                    	//5 for log out data.
						String nick = inStream.readUTF();
						server.getMapUserConnectionOutputStreams().remove(nick);
						server.logOutUser(nick);
						break;
					}
					case 6: {																																			// 6 for friend requests
						String user = inStream.readUTF();
						String friend = inStream.readUTF();
						server.addFriendship(user, friend);
						break;
					}
					case 7: {																																			// 7 for users friends.
						String user = inStream.readUTF();
						ArrayList<String> Friends = server.getFriends(user);
						outStream.writeInt(7);
						for (String friend : Friends)
							outStream.writeUTF(friend);
						outStream.writeUTF("end");
						break;
					}
					case 8: {                                                                      // 8 for Chat Message data
						String recipient = inStream.readUTF();
						String sender = inStream.readUTF();
						String msg = inStream.readUTF();
						server.sendChatMessage(recipient, sender, msg);
						break;
					}
					case 9: {                                                                      // 9 for History with user request
						String sender = inStream.readUTF();
						String friend = inStream.readUTF();
						server.sendHistoryData(sender, friend);
						break;
					}
					case 11: {
						String recipient = inStream.readUTF();
						String sender = inStream.readUTF();
						Long fileSize = inStream.readLong();
						String fileName = inStream.readUTF();

						DataOutputStream RecipientDataOutputStream = server.getRecipientOutputStream(recipient);
						RecipientDataOutputStream.writeInt(11);
						RecipientDataOutputStream.writeUTF(sender);
						RecipientDataOutputStream.writeLong(fileSize);
						RecipientDataOutputStream.writeUTF(fileName);

						ByteArrayOutputStream fos = new ByteArrayOutputStream();
						byte[] buffer = new byte[toIntExact(fileSize)];

						int read;
						int remaining = toIntExact(fileSize);
						while((read = inStream.read(buffer, 0, Math.min(buffer.length, remaining))) > 0) {
							RecipientDataOutputStream.write(buffer);
							remaining -= read;
							fos.write(buffer, 0, read);
						}

						fos.close();
						break;
					}
					case 12: {
						String newOne = inStream.readUTF();
						String oldOne = inStream.readUTF();
						server.updateNick(oldOne, newOne);
						break;
					}
					case 13: {
						String nick = inStream.readUTF();
						String password = inStream.readUTF();
						server.updatePassword(nick, password);
						break;
					}
					case 14: {
						String oldNick = inStream.readUTF();
						String newNick = inStream.readUTF();
						String password = inStream.readUTF();
						server.updateAll(oldNick, newNick, password);
						break;
					}
					default: {
						System.out.println("ERROR IN SWITCH IN SERVER THREAD");
						break;
					}
				}

			} catch (IOException ioe) {
				server.remove(ID);
				break;
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	public void open() throws IOException {
	 	 inStream = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
		 try {
			 outStream = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream())); //try catch jeżeli outstream rzuci wyjątek zamykamy instream
		 } catch (IOException ioe) {
			 ioe.printStackTrace();
			 this.close();
		 }
	}
  public synchronized void close() throws IOException {
  	this.flag = false;
    if (inStream != null) {
	    try {
	    	inStream.close();
	    } catch (IOException ioe) {}
    }
    if (outStream != null) {
      try {
      	outStream.close();
      } catch (IOException ioe) {}
    }
    if (socket != null) {
	    try {
	    	socket.close();
	    } catch (IOException ioe) {}
    }
  }
}
