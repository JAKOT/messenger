package server;
import java.net.*;
import java.io.*;
import java.sql.*;
import java.sql.Date;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MessengerServer implements Runnable {
	private MessengerServerThread clients[] = new MessengerServerThread[50];
  private ServerSocket 					server 		= null;
  private Thread       					thread		= null;
  private int 						 clientCount 		= 0;
  private Connection c = null;
  private static final String dbClassName = "com.mysql.jdbc.Driver";
  private static final String CONNECTION = "jdbc:mysql://127.0.0.1/messenger";
  private ArrayList<User> Users = new ArrayList<>();
	private Map<String, DataOutputStream> mapUserConnectionOutputStreams;

  public MessengerServer(int port) {
		mapUserConnectionOutputStreams = new HashMap<String, DataOutputStream>();
  	try {
  		System.out.println("Binding to port " + port + ", please wait  ...");
  		server = new ServerSocket(port);
  		System.out.println("Server started: " + server);
  		start();
  	} catch (IOException ioe) {
  		ioe.printStackTrace();
  	}
    try {
    	this.connectToDB();
    } catch (SQLException sqle) {
    	System.out.println("SQL EXCEPTION");
    	System.out.println(sqle.getMessage());
    } catch (ClassNotFoundException cnfe) {
    	System.out.println("CLASS NOT FOUND EXCEPTION");
    	System.out.println(cnfe.getMessage());
    }
  }

	public synchronized DataOutputStream getRecipientOutputStream(String recipientNick){
		return mapUserConnectionOutputStreams.get(recipientNick);
	}

	public Map<String, DataOutputStream> getMapUserConnectionOutputStreams() {
		return mapUserConnectionOutputStreams;
	}

  public void run() {
  	while(thread != null) {
  		try {
  			System.out.println("Waiting for a client...");
  			addThread(server.accept());

				Runnable checkLoggedRunnable = new Runnable() {
					public void run() {
						checkAndSendLoggedUsers();
					}
				};

				ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
				executor.scheduleAtFixedRate(checkLoggedRunnable, 5, 10, TimeUnit.SECONDS);

  		} catch (IOException ioe) {
  			ioe.printStackTrace();
  			for(MessengerServerThread cli : clients)
  				remove(cli.getID());			// Removing clients in case of server error before closing server.
  			this.stop();
  		}
  	}
  }
  
  public void start() {
  	if (thread == null) {
  		thread = new Thread(this);
  		thread.start();
  	}
  }
  public void stop() {
  	if (thread != null) {
  		thread.interrupt();
  		thread = null;
  	}
  }
  private int findClient(int ID) {
  	for (int i = 0; i < clientCount; i++)
      if (clients[i].getID() == ID)
         return i;
  	return -1;
  }
  public void closeConnection() { try {
		c.close();
	} catch (SQLException e) {
		e.printStackTrace();
	} }
  
  public void addThread(Socket socket) {
  	if (clientCount < clients.length) {
  		System.out.println("Client accepted: " + socket);
  		clients[clientCount] = new MessengerServerThread(this, socket);
  		try {
  			clients[clientCount].open();
  			clients[clientCount].start();
  			clientCount++;
  		} catch (IOException ioe) {
  			ioe.printStackTrace();
  		}
  	} else System.out.println("Client refused: maximum " + clients.length + " reached.");
  }

  public synchronized void remove(int ID) {
  	int pos = findClient(ID);
  	if (pos >= 0) {
  		MessengerServerThread toTerminate = clients[pos];
  		System.out.println("Removing client thread " + ID + " at " + pos);
  		if (pos < clientCount-1) {
        for (int i = pos+1; i < clientCount; i++)
          clients[i-1] = clients[i];
  		}
  		clientCount--;
  		try {
  			toTerminate.close();
  		} catch (IOException ioe) {
  			ioe.printStackTrace();
  			toTerminate.interrupt();
  		}
  	}
  }

	public synchronized void broadcast(String input, String nick) {
			for (int i = 0; i < clientCount; i++)
				clients[i].send(input, nick);
	}


	public void connectToDB() throws ClassNotFoundException, SQLException {
  	System.out.println(dbClassName);
  	Class.forName(dbClassName);
    // try to connect
    this.c = DriverManager.getConnection(CONNECTION, "root", "foobar");
    System.out.println("Connected to DB...");
    
    showUsers();
            
  }
  
  public void registerUser(String name, String password) throws ClassNotFoundException, SQLException {
		String prpSQL = "INSERT INTO users values(null, ?, ?)";
		PreparedStatement pstmt = c.prepareStatement(prpSQL);
		pstmt.setString(1, name);
		pstmt.setString(2, password);
		pstmt.executeUpdate();
  }

	public void addFriendship(String user, String friend) throws ClassNotFoundException, SQLException {
		int userId = 0;
		int friendId = 0;

		String findUser = "SELECT id FROM users WHERE name=?";
		PreparedStatement pstmt1 = c.prepareStatement(findUser);
		pstmt1.setString(1, user);
		ResultSet userRS = pstmt1.executeQuery();
		if (userRS.next()) userId = userRS.getInt("id");

		String findFriend = "select id from users where name=?";
		PreparedStatement pstmt2 = c.prepareStatement(findFriend);
		pstmt2.setString(1, friend);
		ResultSet friendRS = pstmt2.executeQuery();
		if (friendRS.next()) friendId = friendRS.getInt("id");

		if (userId != 0 && friendId != 0) {
			String SQL1 = "insert into friendships (user, friend) values (?, ?)";
			PreparedStatement pstmt3 = c.prepareStatement(SQL1);
			pstmt3.setInt(1, userId);
			pstmt3.setInt(2, friendId);
			pstmt3.executeUpdate();

			String SQL2 = "insert into friendships (user, friend) values (?, ?)";
			PreparedStatement pstmt4 = c.prepareStatement(SQL2);
			pstmt4.setInt(1, friendId);
			pstmt4.setInt(2, userId);
			pstmt4.executeUpdate();
		}
	}

	public void updateNick(String oldOne, String newOne) throws SQLException {
		String updateNick = "UPDATE users SET name = ? WHERE name = ?";
		PreparedStatement pstmt = c.prepareStatement(updateNick);
		pstmt.setString(1, newOne);
		pstmt.setString(2, oldOne);
		pstmt.executeUpdate();
	}
	public void updatePassword(String nick, String password) throws SQLException {
		String updateNick = "UPDATE users SET password = ? WHERE name = ?";
		PreparedStatement pstmt = c.prepareStatement(updateNick);
		pstmt.setString(1, password);
		pstmt.setString(2, nick);
		pstmt.executeUpdate();
	}
	public void updateAll(String oldNick, String newNick, String password) throws SQLException {
		String updateNick = "UPDATE users SET name = ?, password = ? WHERE name = ?";
		PreparedStatement pstmt = c.prepareStatement(updateNick);
		pstmt.setString(1, newNick);
		pstmt.setString(2, password);
		pstmt.setString(3, oldNick);
		pstmt.executeUpdate();
	}

	public ArrayList<String> getFriends(String user) throws ClassNotFoundException, SQLException {
		ArrayList<String> Friends = new ArrayList<>();
		int userId = 0;

		String findUser = "SELECT id FROM users WHERE name=?";
		PreparedStatement pstmt1 = c.prepareStatement(findUser);
		pstmt1.setString(1, user);
		ResultSet userRS = pstmt1.executeQuery();
		if (userRS.next()) userId = userRS.getInt("id");

		// Finding all the friends of the user with given id.
		String findFriends = "SELECT name AS friend FROM friendships t1 INNER JOIN users t2 ON t2.id = t1.friend WHERE user=?";
		PreparedStatement pstmt2 = c.prepareStatement(findFriends);
		pstmt2.setInt(1, userId);
		ResultSet friendsRS = pstmt2.executeQuery();

		// Adding all the found friend names to the 'Friends' Array
		while(friendsRS.next()) {
			String name = friendsRS.getString("friend");
			Friends.add(name);
		}

		pstmt1.close();
		pstmt2.close();
		return Friends;
	}
  
  public boolean checkUser(String name, String password) throws ClassNotFoundException, SQLException {
  	Statement stmt = c.createStatement();
  	String SQL = "SELECT * FROM users WHERE name LIKE" + "\'" + name + "\'" + " AND password LIKE " + "\'" + password + "\'";  //Prepared statement
  	ResultSet rs = stmt.executeQuery(SQL);
  	if (rs.next()) {
			Users.add(new User(name, password));              //add user to logged in ones.
			showLoggedUsers();
			return true;
		}
  	else return false;
  }

	public void showLoggedUsers() {
		System.out.println("**********************USERS*********************");
		for (User user : Users)
			System.out.println(user.getName());             //showing loggede-on users.
		System.out.println("************************************************");
	}


	public void logOutUser(String name) {
		ArrayList<User> usersToRemove = new ArrayList<>();
		for (User user : Users) {
			if (Objects.equals(user.getName(), name)) {
				System.out.println("Removing user: " + user.getName());
				usersToRemove.add(user);
			}
			showLoggedUsers();
		}
		for(User user : usersToRemove)
			Users.remove(Users.indexOf(user));
		usersToRemove.clear();
	}
  
  public void showUsers() throws ClassNotFoundException, SQLException {
    Statement stmt = c.createStatement();
    ResultSet rs = stmt.executeQuery("SELECT * FROM users");
     
    while (rs.next()){
        int id_col = rs.getInt("ID");
        String name = rs.getString("name");
        String password = rs.getString("password");

        String P = id_col + " " + name + " " + password;
        System.out.println(P);
    }
  }

	public void checkAndSendLoggedUsers() {
		for (String user : mapUserConnectionOutputStreams.keySet()) {
			DataOutputStream RecipientDataOutputStream = getRecipientOutputStream(user);
			try {
				RecipientDataOutputStream.writeInt(10);
				for (String u : mapUserConnectionOutputStreams.keySet())
					RecipientDataOutputStream.writeUTF(u);
				RecipientDataOutputStream.writeUTF("end");
				RecipientDataOutputStream.flush();
			} catch (IOException e) {
				System.out.println("checkAndSendLoggedUsers error: " + e.getMessage());
			}
		}
	}

	public void showFriends() throws ClassNotFoundException, SQLException {
		Statement stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM friendships");

		System.out.println("***************FRIENDSHIPS***********************");
		while (rs.next()) {
			String user = rs.getString("user");
			String friend = rs.getString("friend");

			String P = "Friendship: " + user + ", " + friend;
			System.out.println(P);
		}
		System.out.println("************************************************");
	}

	private void addMessageToDB(String recipient, String sender, String msg) throws SQLException {
		int recipientId = 999;
		int senderId = 999;
		int msgId = 999;
		Statement findRecipientStmt = c.createStatement();
		String findRecipient = "SELECT id FROM users WHERE name LIKE" + "\"" + recipient + "\"";
		ResultSet recipientRS = findRecipientStmt.executeQuery(findRecipient);
		if (recipientRS.next()) recipientId = recipientRS.getInt("id");

		Statement findSenderStmt = c.createStatement();
		String findSender = "SELECT id FROM users WHERE name LIKE" + "\"" + sender + "\"";
		ResultSet senderRS = findSenderStmt.executeQuery(findSender);
		if (senderRS.next()) senderId = senderRS.getInt("id");

		Statement addMsgStmt = c.createStatement();
		String addMsg = "INSERT INTO message (sender_id, content) values (" + senderId + ", " + "\"" + msg + "\"" + ");";
		addMsgStmt.executeUpdate(addMsg);

		Statement findMsgIdStmt = c.createStatement();
		String findMsgId = "SELECT id FROM message WHERE sender_id = " + senderId + " AND " + " content = \"" + msg + "\"";
		ResultSet msgIdRS = findMsgIdStmt.executeQuery(findMsgId);
		if (msgIdRS.next()) msgId = msgIdRS.getInt("id");

		Statement addMsgToStmt = c.createStatement();
		String addMsgTo = "INSERT INTO message_to (message_id, recipient_id, sent) values (" + msgId + ", " + recipientId + ", NOW())";
		addMsgToStmt.executeUpdate(addMsgTo);
	}

	public void sendChatMessage(String recipient, String sender, String msg) throws SQLException {
		DataOutputStream RecipientDataOutputStream = getRecipientOutputStream(recipient);
		if (RecipientDataOutputStream != null) { //wyślij komunikat, że jest niedostępny, może w bazie danych zaległe wiad, albo nie pozwól na wysyłanie
			try {
				RecipientDataOutputStream.writeInt(8);
				RecipientDataOutputStream.writeUTF(sender);
				RecipientDataOutputStream.writeUTF(msg);
				RecipientDataOutputStream.flush();
				addMessageToDB(recipient, sender, msg);
			} catch (IOException e) {
				System.out.println("Server send chat message error: " + e.getMessage());
			}
		}

	}

	public void sendHistoryData(String sender, String friend) throws SQLException, IOException {
		DataOutputStream RecipientDataOutputStream = getRecipientOutputStream(sender);
		int senderID = 0;
		int friendID = 0;
		String findUser = "SELECT id FROM users WHERE name=?";

		PreparedStatement pstmt1 = c.prepareStatement(findUser);
		pstmt1.setString(1, sender);
		ResultSet senderRS = pstmt1.executeQuery();
		if (senderRS.next()) senderID = senderRS.getInt("id");

		PreparedStatement pstmt2 = c.prepareStatement(findUser);
		pstmt1.setString(1, friend);
		ResultSet friendRS = pstmt1.executeQuery();
		if (friendRS.next()) friendID = friendRS.getInt("id");

		Statement stmt = c.createStatement();
		String checkHistory = "SELECT users.name AS sender, content, sent FROM users INNER JOIN message m ON m.sender_id = users.id INNER JOIN message_to mt ON mt.message_id = m.id INNER JOIN users u ON mt.recipient_id = u.id WHERE (mt.recipient_id = " + friendID + "  AND m.sender_id = " + senderID + ") OR (mt.recipient_id = " + senderID + " AND m.sender_id = " + friendID + ") ORDER BY sent ASC";
		ResultSet historyRS = stmt.executeQuery(checkHistory);

		RecipientDataOutputStream.writeInt(9);                      // 9 for history info.
		while (historyRS.next()) {
			String user = historyRS.getString("sender");
			String content = historyRS.getString("content");
			Timestamp sent = historyRS.getTimestamp("sent");
			RecipientDataOutputStream.writeUTF(content);
			RecipientDataOutputStream.writeUTF(user);
			RecipientDataOutputStream.writeUTF(sent.toString());
			RecipientDataOutputStream.flush();

		}
		RecipientDataOutputStream.writeUTF("end");
		RecipientDataOutputStream.flush();
	}
}