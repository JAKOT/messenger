package gui.notificationframe;

import client.controller.Controller;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;

public class NotificationFrame extends JFrame {

  private JLabel text;
  private Controller controller;
  private JPanel container;
  String nick;

  public NotificationFrame() {//(Controller controller) {
    //setController(controller);
    //controller.setNotificationFrame(this);
    initComponents();
    setAlwaysOnTop(true);
    setFocusable(false);
    setVisible(true);
  }

  public void setText(String s) {
    text.setText(s);
  }

  public String getText() {
    return text.getText();
  }

  public void setController(Controller controller) {
    this.controller = controller;
  }

  public void setNick(String s) {
    nick = s;
  }

  private void initComponents() {
    container = new JPanel();
    text = new JLabel();

    setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

    text.setText("jLabel1");

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(container);
    container.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
          .addContainerGap()
          .addComponent(text)
          .addContainerGap(269, Short.MAX_VALUE))
    );
    jPanel1Layout.setVerticalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
          .addGap(35, 35, 35)
          .addComponent(text)
          .addContainerGap(34, Short.MAX_VALUE))
    );

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(container, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
          .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(container, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addContainerGap())
    );

    pack();
  }

  public void addNotificationListener(MouseInputAdapter listenForNotification) {
    container.addMouseListener(listenForNotification);
  }
}
