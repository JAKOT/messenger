package gui.historyframe;


import javax.swing.*;
import client.controller.Controller;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.ArrayList;
import java.util.Date;
import static javax.swing.ScrollPaneConstants.*;

public class HistoryFrame extends JFrame {

  private JPanel boxPanel = new JPanel();
  private JButton ClearHistoryButton;
  private JButton ExportButton;
  private JLabel FriendsLabel;
  private JLabel TitleLabel;
  private JList<String> FriendList;
  private JScrollPane ListScrollPane;
  private JScrollPane HistoryScrollPane;
  private Controller controller;

  public HistoryFrame(Controller controller) {
    initComponents();
    centerComponent(this);
    this.setTitle("History");
    setVisible(true);
    setController(controller);
    controller.setHistoryFrame(this);
    displayFriends(controller.getClientModel().getFriends());
    boxPanel.setLayout(new BoxLayout(boxPanel, BoxLayout.PAGE_AXIS));
    boxPanel.setVisible(true);
  }

  public void setController(Controller controller) {
    this.controller = controller;
  }

  public JList<String> getFriendList() {
    return this.FriendList;
  }

  private void centerComponent(Component frame) {
    Toolkit tk = Toolkit.getDefaultToolkit();
    Dimension dim = tk.getScreenSize();
    int xPos = (dim.width / 2) - (frame.getWidth() / 2);
    int yPos = (dim.height / 2) - (frame.getHeight() / 2);
    frame.setLocation(xPos, yPos);
  }

  public void clearHistoryScrollPane() {
    boxPanel.removeAll();
    boxPanel.revalidate();
    boxPanel.repaint();
  }

  private void initComponents() {

    ListScrollPane = new JScrollPane();
    FriendList = new JList<>();
    FriendsLabel = new JLabel();
    TitleLabel = new JLabel();
    HistoryScrollPane = new JScrollPane(boxPanel);
    ClearHistoryButton = new JButton();
    ExportButton = new JButton();

    HistoryScrollPane.setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);
    setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

    FriendList.setModel(new javax.swing.AbstractListModel<String>() {
      String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
      public int getSize() { return strings.length; }
      public String getElementAt(int i) { return strings[i]; }
    });
    ListScrollPane.setViewportView(FriendList);;

    FriendsLabel.setText("Friends:");

    TitleLabel.setText("History");

    ClearHistoryButton.setText("Clear History");

    ExportButton.setText("Export to file");

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
          .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
            .addComponent(FriendsLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 73, Short.MAX_VALUE)
            .addComponent(ListScrollPane))
          .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
              .addGap(189, 189, 189)
              .addComponent(TitleLabel)
              .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
              .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(HistoryScrollPane)
                .addGroup(layout.createSequentialGroup()
                  .addComponent(ClearHistoryButton)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 357, Short.MAX_VALUE)
                  .addComponent(ExportButton)))
              .addContainerGap())))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
          .addContainerGap()
          .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
            .addComponent(FriendsLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(TitleLabel))
          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
          .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
              .addComponent(HistoryScrollPane)
              .addGap(18, 18, 18)
              .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(ClearHistoryButton)
                .addComponent(ExportButton)))
            .addComponent(ListScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 357, javax.swing.GroupLayout.PREFERRED_SIZE))
          .addContainerGap())
    );

    pack();
  }


  public void addMessage(String msg, String nick, String sent) {
    if(msg.length()!=0) {
      JLabel label = new JLabel();
      label.setOpaque(true);
      label.setBackground(new Color(240, 240, 240));
      label.setText("<html><p style=\"width:400px;\">" + "[<span style=\"color:#00b3b3;font-weight:bold;\">" + nick + "</span>" + sent + "] " + msg + "</p></html>");
      label.setForeground(Color.BLACK);
      boxPanel.add(label);
      boxPanel.add(Box.createRigidArea(new Dimension(0,5)));
      boxPanel.revalidate();
      final JScrollBar verticalBar = HistoryScrollPane.getVerticalScrollBar();
      AdjustmentListener downScroller = new AdjustmentListener() {
        public void adjustmentValueChanged(AdjustmentEvent e) {
          Adjustable adjustable = e.getAdjustable();
          adjustable.setValue(adjustable.getMaximum());
          verticalBar.removeAdjustmentListener(this); // need this so it does not stop user from scrolling up
        }
      };
      verticalBar.addAdjustmentListener(downScroller);
    }
  }

  public void addFriendListListener(ListSelectionListener listenForFriendList) {
    FriendList.addListSelectionListener(listenForFriendList);
  }

  public void addExportButtonListener(ActionListener listenForExportButton) {
    ExportButton.addActionListener(listenForExportButton);
  }

  public void displayFriends(ArrayList<String> Friends) {
    DefaultListModel list = new DefaultListModel();
    for (String friend : Friends) {
      list.addElement(friend);
    }
    FriendList.setModel(list);
  }

}
