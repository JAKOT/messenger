package gui.registerframe;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import client.controller.Controller;
import gui.loginframe.LoginFrame;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RegisterFrame extends JFrame {
  private Controller controller;  
  private JButton RegisterButton;
  private JButton LoginButton;
  private JLabel HeaderLabel;
  private JLabel NameLabel;
  private JLabel PasswordLabel;
  private JLabel ToLoginLabel;
  private JTextField NameField;
  private JTextField PasswordField;
  
	public RegisterFrame(Controller controller) {
    initComponents();
		setController(controller);
		getController().setRegisterFrame(this);
    centerComponent(this);
    this.setTitle("Register");
  }
  
	private void centerComponent(Component frame) {
    Toolkit tk = Toolkit.getDefaultToolkit();
    Dimension dim = tk.getScreenSize();
    int xPos = (dim.width / 2) - (frame.getWidth() / 2);
    int yPos = (dim.height / 2) - (frame.getHeight() / 2);
    frame.setLocation(xPos, yPos);
  }
	
	public void setController(Controller controller) {
		this.controller = controller;
	}
	
	public Controller getController() {
		return controller;
	}

  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
  private void initComponents() {

    HeaderLabel = new JLabel();
    NameLabel = new JLabel();
    PasswordLabel = new JLabel();
    NameField = new JTextField();
    PasswordField = new JTextField();
    RegisterButton = new JButton();
    ToLoginLabel = new JLabel();
    LoginButton = new JButton();

    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    HeaderLabel.setFont(new Font("Ubuntu", 1, 18)); // NOI18N
    HeaderLabel.setText("Create new account!");

    NameLabel.setText("Nickname:");

    PasswordLabel.setText("Password:");

    NameField.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        NameFieldActionPerformed(evt);
      }
    });

    RegisterButton.setFont(new Font("Ubuntu", 1, 18)); // NOI18N
    RegisterButton.setText("Create");
    
    /*RegisterButton.addActionListener(new ActionListener() {
    	public void actionPerformed(ActionEvent evt) {
    		RegisterButtonActionPerformed(evt);
    	}
    });*/

    LoginButton.addActionListener(new ActionListener() {
    	public void actionPerformed(ActionEvent evt) {
        LoginButtonActionPerformed(evt);
      }
    });
    
    ToLoginLabel.setFont(new java.awt.Font("Ubuntu", 0, 12)); // NOI18N
    ToLoginLabel.setText("Already have an account?");

    LoginButton.setText("Login");

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
        .addContainerGap(150, Short.MAX_VALUE)
        .addComponent(HeaderLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGap(100, 100, 100))
      .addGroup(layout.createSequentialGroup()
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(layout.createSequentialGroup()
            .addGap(47, 47, 47)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
              .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                  .addComponent(PasswordLabel)
                  .addComponent(NameLabel))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                  .addComponent(NameField, javax.swing.GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE)
                  .addComponent(PasswordField)))
              .addGroup(layout.createSequentialGroup()
                .addComponent(LoginButton)
                .addGap(94, 94, 94)
                .addComponent(RegisterButton, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))))
          .addGroup(layout.createSequentialGroup()
            .addGap(65, 65, 65)
            .addComponent(ToLoginLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addGap(34, 34, 34)
        .addComponent(HeaderLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGap(37, 37, 37)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(NameLabel)
          .addComponent(NameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addGap(26, 26, 26)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(PasswordLabel)
          .addComponent(PasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
        .addComponent(ToLoginLabel)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(RegisterButton, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(LoginButton))
        .addGap(39, 39, 39))
    );

    pack();
  }// </editor-fold>                        

	private void LoginButtonActionPerformed(ActionEvent evt) {    // Back to login                                   
	  setVisible(false);
	  dispose();
	  LoginFrame login = new LoginFrame(controller);
	  login.setVisible(true);
	}
	
	public void addRegisterButtonListener(ActionListener listenForRegisterButton){
       RegisterButton.addActionListener(listenForRegisterButton);        
	}
	
	public void userRegisteredDialog() {
		JOptionPane.showMessageDialog(this, "User successfully registrated, feel free to login now!", "", JOptionPane.PLAIN_MESSAGE);
	}
	
	public void invalidRegistrationDialog() {
		JOptionPane.showMessageDialog(this, "Username or password too short.", "Invalid data", JOptionPane.ERROR_MESSAGE);
	}
	
	/*private void RegisterButtonActionPerformed(ActionEvent evt) {
		// Register a user
		String name = NameField.getText();
		String password = PasswordField.getText();
		controller.registerUser(name, password);
	}*/
	
  private void NameFieldActionPerformed(ActionEvent evt) {                                            
  }                     
  public String getNameField() {
  	return NameField.getText();
  }
  
  public String getPasswordField() {
  	return PasswordField.getText();
  }
          
}
