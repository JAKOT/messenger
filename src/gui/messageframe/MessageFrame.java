package gui.messageframe;

import client.controller.Controller;
import client.view.MainFrame;
import gui.imageviewframe.ImageViewFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;

public class MessageFrame extends JFrame {
  private JButton sendButton;
  private JScrollPane ChatScrollPane;
  private JPanel boxPanel = new JPanel();
  private JScrollPane jScrollPane2;
  private JTextArea MessageArea;
  private JLabel BoxTitle;
  private String friend;
  private Controller controller;
  private JButton fileButton;

  public MessageFrame(Controller controller, String friend) {
    initComponents();
    setController(controller);
    this.friend = friend;
    controller.addChatFrame(friend, this);
    setVisible(true);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

    BoxTitle.setText(controller.getUserNick() + " Chat with " + friend);
    MessageArea.setLineWrap(true);
    boxPanel.setLayout(new BoxLayout(boxPanel, BoxLayout.PAGE_AXIS));
    boxPanel.setVisible(true);
    centerComponent(this);
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent windowEvent) {
        controller.getClientModel().removeChatFrame(friend);
        controller.removeChatFrame(friend);
        super.windowClosing(windowEvent);
      }
    });
  }

  public void clearChatScrollPane() {
    boxPanel.removeAll();
    boxPanel.revalidate();
    boxPanel.repaint();
  }

  public MessageFrame(Controller controller, String friend, String msg) {
    initComponents();
    setController(controller);
    this.friend = friend;
    controller.addChatFrame(friend, this);
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);

    BoxTitle.setText(controller.getUserNick() + " Chat with " + friend);
    MessageArea.setLineWrap(true);
    boxPanel.setLayout(new BoxLayout(boxPanel, BoxLayout.PAGE_AXIS));
    boxPanel.setVisible(true);
    centerComponent(this);

    addMessage(msg, friend);

    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent windowEvent) {
        controller.getClientModel().removeChatFrame(friend);
        controller.removeChatFrame(friend);
        super.windowClosing(windowEvent);
      }
    });
  }

  public void centerComponent(Component frame) {
    Toolkit tk = Toolkit.getDefaultToolkit();
    Dimension dim = tk.getScreenSize();
    int xPos = (dim.width / 2) - (frame.getWidth() / 2);
    int yPos = (dim.height / 2) - (frame.getHeight() / 2);
    frame.setLocation(xPos, yPos);
  }

  public void setController(Controller controller) {
    this.controller = controller;
  }

  public Controller getController() {
    return this.controller;
  }

  public String getFriend() {
    return friend;
  }

  public String getMessage() {
    return MessageArea.getText().trim();
  }


  public JTextArea getMessageArea() {
    return MessageArea;
  }

  public void addMessage(String _msg, String nick) {
    String msg = _msg;
    if(true) {
      JLabel label = new JLabel();
      label.setOpaque(true);
      label.setBackground(new Color(240, 240, 240));
      label.setText("<html><p style=\"width:400px;\">" + "[<span style=\"color:#00b3b3;font-weight:bold;\">" + nick + "</span>] " + msg + "</p></html>");
      //label.setForeground(Color.BLACK);
      boxPanel.add(label);
      boxPanel.add(Box.createRigidArea(new Dimension(0,5)));
      MessageArea.setText("");
      boxPanel.revalidate();
      final JScrollBar verticalBar = ChatScrollPane.getVerticalScrollBar();
      AdjustmentListener downScroller = new AdjustmentListener() {
        public void adjustmentValueChanged(AdjustmentEvent e) {
          Adjustable adjustable = e.getAdjustable();
          adjustable.setValue(adjustable.getMaximum());
          verticalBar.removeAdjustmentListener(this); // need this so it does not stop user from scrolling up
        }
      };
      verticalBar.addAdjustmentListener(downScroller);
    }
  }

  public void addImage(String fileName, String nick) {
    ImageIcon icon= new ImageIcon(fileName);
    addMessage(fileName + " saved to " + (new File(fileName).getAbsolutePath()), nick);
    JLabel label = new JLabel();
    icon.setImage(icon.getImage().getScaledInstance(150, -1 , Image.SCALE_DEFAULT));
    label.setIcon(icon);
    label.setOpaque(true);
    label.setBackground(new Color(240, 240, 240));
    //label.setHorizontalTextPosition(JLabel);
    label.setVerticalTextPosition(JLabel.NORTH);
    label.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        try {
          new ImageViewFrame(fileName);
        } catch (Exception e1) {
          e1.printStackTrace();
        }
      }
    });
    boxPanel.add(label);
    boxPanel.add(Box.createRigidArea(new Dimension(0,5)));
    boxPanel.revalidate();
    final JScrollBar verticalBar = ChatScrollPane.getVerticalScrollBar();
    AdjustmentListener downScroller = new AdjustmentListener() {
      public void adjustmentValueChanged(AdjustmentEvent e) {
        Adjustable adjustable = e.getAdjustable();
        adjustable.setValue(adjustable.getMaximum());
        verticalBar.removeAdjustmentListener(this); // need this so it does not stop user from scrolling up
      }
    };
    verticalBar.addAdjustmentListener(downScroller);
  }

  public void addImageWithPath(String path, String nick) {
    ImageIcon icon = new ImageIcon(path);
    addMessage("", nick);
    JLabel label = new JLabel();
    icon.setImage(icon.getImage().getScaledInstance(150, -1 , Image.SCALE_DEFAULT));
    label.setIcon(icon);
    label.setOpaque(true);
    label.setBackground(new Color(240, 240, 240));
    //label.setHorizontalTextPosition(JLabel);
    label.setVerticalTextPosition(JLabel.NORTH);
    label.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        try {
          new ImageViewFrame(path);
        } catch (Exception e1) {
          e1.printStackTrace();
        }
      }
    });
    boxPanel.add(label);
    boxPanel.add(Box.createRigidArea(new Dimension(0,5)));
    boxPanel.revalidate();
    final JScrollBar verticalBar = ChatScrollPane.getVerticalScrollBar();
    AdjustmentListener downScroller = new AdjustmentListener() {
      public void adjustmentValueChanged(AdjustmentEvent e) {
        Adjustable adjustable = e.getAdjustable();
        adjustable.setValue(adjustable.getMaximum());
        verticalBar.removeAdjustmentListener(this); // need this so it does not stop user from scrolling up
      }
    };
    verticalBar.addAdjustmentListener(downScroller);
  }

  private void initComponents() {

    BoxTitle = new JLabel();
    sendButton = new JButton();
    fileButton = new JButton();
    ChatScrollPane = new JScrollPane(boxPanel);
    jScrollPane2 = new JScrollPane();
    MessageArea = new JTextArea();

    setResizable(false);

    sendButton.setText("Send");
    fileButton.setText("Send file");

    MessageArea.setColumns(20);
    MessageArea.setRows(2);
    jScrollPane2.setViewportView(MessageArea);

    BoxTitle.setForeground(new java.awt.Color(51, 204, 255));
    BoxTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    BoxTitle.setText("BoxTitle");

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
          .addContainerGap()
          .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ChatScrollPane)
            .addGroup(layout.createSequentialGroup()
              .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 467, Short.MAX_VALUE)
              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
              .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(sendButton, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                .addComponent(fileButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
              .addGap(6, 6, 6)))
          .addContainerGap())
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
          .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(BoxTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addGap(231, 231, 231))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
          .addGap(23, 23, 23)
          .addComponent(BoxTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
          .addComponent(ChatScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addGap(18, 18, 18)
          .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createSequentialGroup()
              .addComponent(sendButton)
              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
              .addComponent(fileButton)))
          .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    pack();
  }


  public void addSendButtonListener(ActionListener listenForSendButton) {
    sendButton.addActionListener(listenForSendButton);
  }

  public void addFileButtonListener(ActionListener listenForFileButton) {
    fileButton.addActionListener(listenForFileButton);
  }



}
