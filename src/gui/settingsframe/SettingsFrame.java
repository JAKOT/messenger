package gui.settingsframe;
import client.controller.Controller;

import java.awt.Adjustable;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.TextArea;
import java.awt.Toolkit;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.text.DefaultCaret;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SettingsFrame extends JFrame {

  private JButton applyButton;
  private JCheckBox notificationsCheckBox;
  private JCheckBox loginChecckBox;
  private JLabel jLabel13;
  private JLabel jLabel14;
  private JPanel jPanel7;
  private JTextField nameField;
  private JTextField passwordField;
  private Controller controller;

	 public SettingsFrame(Controller controller) {
     setController(controller);
     initComponents();
     controller.setSettingsFrame(this);
     nameField.setText(controller.getUserNick());
     centerComponent(this);
     this.setResizable(false);
     this.setTitle("Settings");
		}


  public void setController(Controller controller) {
    this.controller = controller;
  }

  public JCheckBox getNotificationsCheckBox() {
    return notificationsCheckBox;
  }

  public JTextField getNameField() {
    return nameField;
  }

  public JTextField getPasswordField() {
    return passwordField;
  }

		private void centerComponent(Component frame) {
     Toolkit tk = Toolkit.getDefaultToolkit();
     Dimension dim = tk.getScreenSize();
     int xPos = (dim.width / 2) - (frame.getWidth() / 2);
     int yPos = (dim.height / 2) - (frame.getHeight() / 2);
     frame.setLocation(xPos, yPos);
   }

   private void initComponents() {

     jPanel7 = new javax.swing.JPanel();
     jLabel13 = new javax.swing.JLabel();
     jLabel14 = new javax.swing.JLabel();
     nameField = new javax.swing.JTextField();
     passwordField = new javax.swing.JTextField();
     notificationsCheckBox = new javax.swing.JCheckBox();
     loginChecckBox = new javax.swing.JCheckBox();
     applyButton = new javax.swing.JButton();

     notificationsCheckBox.setSelected(controller.getClientModel().getNotifications());
     setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
     setMaximumSize(new java.awt.Dimension(400, 350));
     setMinimumSize(new java.awt.Dimension(400, 350));
     setPreferredSize(new java.awt.Dimension(400, 350));
     setSize(new java.awt.Dimension(500, 400));

     jPanel7.setMaximumSize(new java.awt.Dimension(400, 350));
     jPanel7.setMinimumSize(new java.awt.Dimension(400, 350));
     jPanel7.setPreferredSize(new java.awt.Dimension(400, 350));

     jLabel13.setText("Nickname:");

     jLabel14.setText("Password:");

     notificationsCheckBox.setText("Notifications");

     loginChecckBox.setText("Login on startup");

     applyButton.setText("Save changes");
     applyButton.setMaximumSize(new java.awt.Dimension(120, 30));
     applyButton.setMinimumSize(new java.awt.Dimension(120, 30));
     applyButton.setPreferredSize(new java.awt.Dimension(120, 30));

     notificationsCheckBox.addActionListener(new ActionListener() {
       @Override
       public void actionPerformed(ActionEvent event) {
         JCheckBox cb = (JCheckBox) event.getSource();
         if (cb.isSelected()) {
           controller.getClientModel().setNotifications(true);
         } else {
           controller.getClientModel().setNotifications(false);
         }
       }
     });
     
     javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
     jPanel7.setLayout(jPanel7Layout);
     jPanel7Layout.setHorizontalGroup(
       jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
       .addGroup(jPanel7Layout.createSequentialGroup()
         .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
           .addGroup(jPanel7Layout.createSequentialGroup()
             .addGap(36, 36, 36)
             .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(jPanel7Layout.createSequentialGroup()
                 .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                   .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                   .addComponent(jLabel14))
                 .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                 .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                   .addComponent(nameField)
                   .addComponent(passwordField, javax.swing.GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE)))
               .addGroup(jPanel7Layout.createSequentialGroup()
                 .addGap(97, 97, 97)
                 .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                   .addComponent(notificationsCheckBox)
                   .addComponent(loginChecckBox)))))
           .addGroup(jPanel7Layout.createSequentialGroup()
             .addGap(140, 140, 140)
             .addComponent(applyButton, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
         .addContainerGap(65, Short.MAX_VALUE))
     );
     jPanel7Layout.setVerticalGroup(
       jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
       .addGroup(jPanel7Layout.createSequentialGroup()
         .addGap(42, 42, 42)
         .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
           .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
           .addComponent(nameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
         .addGap(18, 18, 18)
         .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
           .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
           .addComponent(passwordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
         .addGap(47, 47, 47)
         .addComponent(notificationsCheckBox)
         .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
         .addComponent(loginChecckBox)
         .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
         .addComponent(applyButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
         .addGap(44, 44, 44))
     );

     javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
     getContentPane().setLayout(layout);
     layout.setHorizontalGroup(
       layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
       .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
         .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
         .addGap(0, 0, Short.MAX_VALUE))
     );
     layout.setVerticalGroup(
       layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
       .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, 357, Short.MAX_VALUE)
     );

     pack();
   }// </editor-fold>                        

  public void addApplyButtonListener(ActionListener applyButtonListener) {
    applyButton.addActionListener(applyButtonListener);
  }


}
