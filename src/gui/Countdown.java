package gui;
import client.model.ClientModel;
import gui.notificationframe.NotificationFrame;

import java.util.Timer;
import java.util.TimerTask;

public class Countdown {
  NotificationFrame window;
  ClientModel client;
  Timer timer;

  public Countdown(NotificationFrame frame, ClientModel client)  {
    this.client = client;
    this.window = frame;
    timer = new Timer();
    timer.schedule(bTicking, 1, 1000);
  }

  TimerTask bTicking = new TimerTask() {
    public void run() {
      notificationTimer();
    }
  };

  int seconds = 5;
  private void notificationTimer() {

    System.out.println(seconds);

    seconds--;
    if(seconds<=0) {
      window.dispose();
      client.setNotificationFrame(null);
      timer.cancel();
      timer.purge();
      return;
    }
  }
}
