package gui.addframe;


import client.controller.Controller;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionListener;


public class AddFrame extends JFrame {
  private Controller controller;
  private JButton addButton;
  private JLabel jLabel1;
  private JTextField friendNameField;
  private JButton fileSelectorButton;

  public void setController(Controller controller) {
    this.controller = controller;
  }

  public void clearFriendNameField() {
    friendNameField.setText("");
  }

  public Controller getController() {
    return this.controller;
  }

  public String getFriendNameField() {
    return friendNameField.getText();
  }

  public AddFrame(Controller controller) {
    initComponents();
    setController(controller);
    getController().setAddFrame(this);
    centerComponent(this);
    this.setResizable(false);
    this.setTitle("Add Contact");

  }

  private void centerComponent(Component frame) {
    Toolkit tk = Toolkit.getDefaultToolkit();
    Dimension dim = tk.getScreenSize();
    int xPos = (dim.width / 2) - (frame.getWidth() / 2);
    int yPos = (dim.height / 2) - (frame.getHeight() / 2);
    frame.setLocation(xPos, yPos);
  }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
  private void initComponents() {

    addButton = new JButton();
    jLabel1 = new JLabel();
    friendNameField = new JTextField();
    fileSelectorButton = new JButton();

    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("Add contact");
    setAlwaysOnTop(true);

    jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
    jLabel1.setText("Nick:");
    addButton.setText("Add");
    friendNameField.setBorder(new LineBorder(new java.awt.Color(0, 194, 179), 1, true));

    fileSelectorButton.setText("Add Friends from file");

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
          .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(fileSelectorButton)
          .addGap(133, 133, 133))
        .addGroup(layout.createSequentialGroup()
          .addGap(40, 40, 40)
          .addComponent(jLabel1)
          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
          .addComponent(friendNameField, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
          .addComponent(addButton)
          .addContainerGap())
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
          .addGap(29, 29, 29)
          .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
            .addComponent(friendNameField, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jLabel1)
            .addComponent(addButton, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
          .addComponent(fileSelectorButton)
          .addGap(27, 27, 27))
    );

    pack();
  }

  public void addFileSelectorButtonListener(ActionListener listenForFileSelectorButton) {
    fileSelectorButton.addActionListener(listenForFileSelectorButton);
  }

  public void addaddButtonListener(ActionListener listenForAddButton){
    addButton.addActionListener(listenForAddButton);
  }                                 

}