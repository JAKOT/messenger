package gui.loginframe;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;

import client.controller.Controller;
import client.view.MainFrame;
import gui.registerframe.RegisterFrame;
import server.MessengerServer;

public class LoginFrame extends JFrame {
	  private JButton RegisterButton;
	  private JButton LoginButton;
	  private JLabel PasswordLabel;
	  private JLabel NameLabel;
	  private JLabel HeaderLabel;
	  private JPasswordField PasswordField;
	  private JTextField NameField;
	  private Controller controller;
	  
    public LoginFrame(Controller controller) {
        initComponents();
    		setController(controller);
    		getController().setLoginFrame(this);
        centerComponent(this);
        this.setTitle("Login");
    }
    
    
  	public void setController(Controller controller) {
  		this.controller = controller;
  	}
  	
  	public Controller getController() {
  		return controller;
  	}
  	
    private void centerComponent(Component frame) {
      Toolkit tk = Toolkit.getDefaultToolkit();
      Dimension dim = tk.getScreenSize();
      int xPos = (dim.width / 2) - (frame.getWidth() / 2);
      int yPos = (dim.height / 2) - (frame.getHeight() / 2);
      frame.setLocation(xPos, yPos);
      
    }

    private void initComponents() {

      PasswordLabel = new JLabel();
      PasswordField = new JPasswordField();
      RegisterButton = new JButton();
      NameLabel = new JLabel();
      HeaderLabel = new JLabel();
      NameField = new JTextField();
      LoginButton = new JButton();

      setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
      setTitle("Login");
      setAlwaysOnTop(true);
      setBackground(new Color(200, 200, 200));
      setFont(new Font("Arial", 1, 12)); // NOI18N
      setMaximumSize(new Dimension(400, 300));
      setPreferredSize(new Dimension(400, 300));
      setSize(new Dimension(400, 300));

      PasswordLabel.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
      PasswordLabel.setText("Password:");

      RegisterButton.setFont(new Font("Ubuntu", 1, 12)); // NOI18N
      RegisterButton.setText("Register");

      NameLabel.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
      NameLabel.setText("Nickname:");

      HeaderLabel.setFont(new Font("Ubuntu", 1, 18)); // NOI18N
      HeaderLabel.setText("Log in!");

      LoginButton.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
      LoginButton.setText("Login");

      RegisterButton.addActionListener(new ActionListener() {
      	public void actionPerformed(ActionEvent evt) {
          RegisterButtonActionPerformed(evt);
        }
      });

      GroupLayout layout = new GroupLayout(getContentPane());
      getContentPane().setLayout(layout);
      layout.setHorizontalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
          .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
              .addContainerGap()
              .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(PasswordLabel)
                .addComponent(NameLabel))
              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
              .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                  .addComponent(RegisterButton, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addGap(53, 53, 53)
                  .addComponent(LoginButton, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                  .addComponent(PasswordField)
                  .addComponent(NameField, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)))
              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
              .addGap(166, 166, 166)
              .addComponent(HeaderLabel)))
          .addGap(46, 46, 46))
      );
      layout.setVerticalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
          .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
              .addGap(26, 26, 26)
              .addComponent(HeaderLabel))
            .addGroup(layout.createSequentialGroup()
              .addGap(76, 76, 76)
              .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(NameLabel)
                .addComponent(NameField, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))))
          .addGap(12, 12, 12)
          .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
            .addComponent(PasswordLabel)
            .addComponent(PasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
          .addGap(35, 35, 35)
          .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
            .addComponent(RegisterButton, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(LoginButton, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
          .addContainerGap(52, Short.MAX_VALUE))
      );

      pack();
    }                     

    private void textField1ActionPerformed(ActionEvent evt) {
    	
    }                                                        

    private void RegisterButtonActionPerformed(ActionEvent evt) {
      setVisible(false);
      dispose();
      RegisterFrame register = new RegisterFrame(controller);
      register.setVisible(true);
    }   
    
    private void textField1FocusGained(FocusEvent evt) {                                       

    }
    
  	public void invalidLoginDialog() {
  		JOptionPane.showMessageDialog(this, "Wrong username or password", "Invalid login", JOptionPane.ERROR_MESSAGE);
  	}
    
  	public void addLoginButtonListener(ActionListener listenForLoginButton){
      LoginButton.addActionListener(listenForLoginButton);        
  	}
  	
  	public String getNameField() {
  		return NameField.getText().trim();
  	}
  	
  	public String getPasswordField() {
  		return PasswordField.getText().trim();
  	}
}