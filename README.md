## Autor: Jakub Kopyś Gr. Lab. 4 rok II IS WIMIiP ##
 Wyrażam zgodę na opublikowanie mojego kodu w celach edukacyjnych
OPIS PROJEKTU:

Messenger jest to aplkajca umożliwiająca użytkownikowi założenie konta, zalogowania i prowadzenia
konwersacji z innymi użytkownikami. Rozmowa może być prowadzaona na zasadzie rozmowy rozgłoszeniowej
do wszystkich, lub rozmowy prywatnej między dwoma użytkownikami. Jest możliwość wysyłania plików, pliki o
rozszerzeniu jpg, png, gif itp zostają wyświetlone w oknie rozmowy. Dodatkowo użytkownik o otrzymanej
wiadomości jest powiadamiany sygnałem dźwiękowym oraz powiadomieniem w rogu ekranu. Istnieje także
możliwośc zmiany nicku, hasła, wyłączenia powiadomień dźwiękowych. Wszelkie rozmowy prywatne zapisywane
są do bazy danych – użytkownik może przeglądać historię z dowolnym znajomym.


Harmonogram:

Przgotowanie GUI korzystając ze SWING:

* 	 Okno logowania oraz rejestracji                                  **X 15.03.2016** 
* 	 Okno główne komunikatora                                       **X 15.03.2016**
* 	 Okno ustawień/historii/dodawania kontaktów                        **X 15.03.2016**
* 	 Obsługa okien                                                      **X 15.03.2016**

 Zapis I odczyt plików:

* 	 zapisywanie błędów(np. wysyłanych przez kilenta serwerowi - logs) do pliku   **X 17.05.2016**
* 	 Pobranie historii rozmów do pliki tekstowego **X 24.05.2016r**
* 	 Zapisywanie listy kontaktów do pliku tekstowego  **X 17.05.2016**
* 	 Dodawnie kontaktów z pliku tekstowego   **X 17.05.2016**

Bazy danych:

* 	 zaprojektowanie bazy danych I implementacja połączenia z nią **X 12.04.16r**
* 	 dodawanie historii rozmów do bazy **X 24.05.2016r**
* 	 rejestracja i logowanie do aplikacji **X 12.04.16r**
* 	 dodawanie kontaktów i wyświetlanie ich w liście **X 10.05.2016r**

Współbieżność:

* 	synchronizacja współdzielnych obiektów  **X 31.05.2016r**
* 	wielodostępowy server (writualne zebranie) **X 10.05.2016r**
* 	sprawdzanie dostępnych kontaktów w osobnym wątku **X 24.05.2016r**
* 	wątek u kilenta do kontroli przy oodebranych wiadomościach (oczekiwanie na wiadomosci).  **X 31.05.2016r**

Połączenie Client/Server:

* 	stworzenie kilenta **X 05.04.2016r**
* 	stworzenie serwera **X 05.04.2016r**
* 	komunikacja kilent-serwer (rozmowa w trybie rozgłoszeniowym) **X 10.05.2016r**
* 	rozmowy prywatne **X 10.05.2016r**

Zaproponowane przez studenta:

* 	wyświetlanie img w oknie **X 31.05.2016r**
* 	powiadomienia o otrzymanej wiadomości **X 17.05.2016**
* 	ustawienia **!!!!!!!!!!!!!!!!**
*       przesyłanie plików **X 31.05.2016r**

![Screenshot_20160604_155352.png](https://bitbucket.org/repo/ErkErr/images/3127415509-Screenshot_20160604_155352.png)

![Screenshot_20160604_155413.png](https://bitbucket.org/repo/ErkErr/images/2213160471-Screenshot_20160604_155413.png)

![Screenshot_20160604_155431.png](https://bitbucket.org/repo/ErkErr/images/586611299-Screenshot_20160604_155431.png)

![Screenshot_20160604_155512.png](https://bitbucket.org/repo/ErkErr/images/2682173667-Screenshot_20160604_155512.png)

![Screenshot_20160604_155547.png](https://bitbucket.org/repo/ErkErr/images/3953753509-Screenshot_20160604_155547.png)

![Screenshot_20160604_155711.png](https://bitbucket.org/repo/ErkErr/images/4252318533-Screenshot_20160604_155711.png)

![Screenshot_20160604_155748.png](https://bitbucket.org/repo/ErkErr/images/2322376824-Screenshot_20160604_155748.png)

![Screenshot_20160604_155801.png](https://bitbucket.org/repo/ErkErr/images/1905830789-Screenshot_20160604_155801.png)

![Screenshot_20160604_155838.png](https://bitbucket.org/repo/ErkErr/images/3018707567-Screenshot_20160604_155838.png)

![Screenshot_20160604_160246.png](https://bitbucket.org/repo/ErkErr/images/1709902755-Screenshot_20160604_160246.png)